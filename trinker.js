module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        return p.filter( function(pers){
            if(pers.gender === "Male"){
                return true
            }else{
                return false
            }
        } )
    },


    // allFemale: function(p){
    //     return p.filter( function(pers){
    //         if(pers.gender === "Female"){
    //             return true
    //         }else{
    //             return false
    //         }
    //     } )
    // },
ByGenre: function(p, g){
    return p.filter( function(pers){
        if(pers.gender === g){
            return true
        }else{
            return false 
        }
       
           
               
            })
           
        },

    

    // nbOfMale: function(p){
    //     return this.ByGenre(p, "male").length;
    // },
    nbofpepole: function(p){
        return this.p(first_name,last_name).length;

    },
  


    nbOfFemale: function(p){
        return this.ByGenre(p), "Female".length;
    },

    
    nb: function(p, filtre,param){
        let filtré = filtre(p, param)
        return filtré.length
    },

    // nbOfFemaleInterest: function(p){
    //     let touscherchefemme= p.filter( function(pers){
    //         if(pers.looking_for === "M"){
    //             return true
    //         }else{
    //             return false
    //         }
    //     } )
    //     return touscherchefemme.length
    // },

   ByInterest: function(p,a){
        let touscherchehomme = p.filter( function(pers){
            if(pers.looking_for === a){
                return true
            }else{
                return false
            }
        } )
        return touscherchehomme;
    },

    match: function(p){
        return "not implemented".red;
    },


nbPersonMore2000: function(p,n){
    let tousplus2000= p.filter( function(pers){
        if(parseFloat(pers.income.slice(1)) === n){//parsefloat converti les chaine de caractere en entier. Slice permet de couper des caractéres.aussi.replace("$",") erol
            return true
        }else{
            return false
        }
    } )
    return 'salut';
},


byIncome: function(p,a){
    let tousplus2000= p.filter( function(pers){
        if(parseFloat(pers.income.slice(1)) > a){//aussi.replace("$",") erol
            return true
        }else{
            return false
        }
    } )
    return tousplus2000;
},


ByMovie: function(p,m){
    let touspersonlovedrama= p.filter( function(pers){
        if(pers.pref_movie.includes(m)){ 

      
            return true
        }else{
            return false
        }
    } )
    return touspersonlovedrama;
},

nbdefemmeaimesciencefiction: function(p,n){
    let tousfemmeaimesciencefiction = this.allFemale(p).filter( function(pers){
        if(pers.pref_movie.includes(n)){
            return true
        }else{
            return false
        }
    } )
    return tousfemmeaimesciencefiction.length;
},

allFemale: function(p){
        return p.filter( function(pers){
            if(pers.gender === "Female"){
                return true
            }else{
                return false
            }
        } )
    },

   chercheNom:function(p){
        let R = []
        for(let i of p){
            let isimple ={
                id:i.id,
                name:i.first_name + i.last_name,
                income:i.income,
            }
            R.push(isimple)
        }
        return R
    },

    plusRiche:function(p){
        let R=[]
        for(let personne of p){
            salaire = parseFloat(personne.income.slice(1))
            R.push({salaire: salaire, id: personne.id, nom: personne.last_name})
        }
        R.sort((a,b)=> a.salaire - b.salaire);
        return R
    },
    salairemoyen:function(p){
        somme = 0
        for(let personne of p){
        somme = somme + personne.salaire
       }
       nbdepersonne=p.length
       moyenne = somme/nbdepersonne
       return moyenne 
    },
    salairemedian: function(p){
        if(p.length % 2 === 0){
            taille1 = (p.length / 2) - 1
            taille2 = (p.length / 2)       
            return ((p[taille1].salaire) + (p[taille2].salaire))/2
        }
        else{
            taille = (p.length/2) - 0,5
            return p[taille].salaire
        }

    },
    personnedunord: function(tableau){
        return tableau.filter(elem => elem.latitude > 0)
    },
    personnedusud: function(tableau){
        return tableau.filter(elem => elem.latitude < 0)
    },

}

